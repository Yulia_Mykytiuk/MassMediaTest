function mainSlider(selector) {
	$(selector).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: false,
		arrows: true,
		infinite: true,
		responsive: [
			{
				breakpoint: 479,
			    settings: {
			        arrows: false,
			        autoplay: true
			    }
			}
		]
	});
}

function reviewsSlider(selector) {
	$(selector).slick({
		centerMode: true,
		centerPadding: '60px',
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		infinite: true,
		responsive: [
			{
				breakpoint: 767,
			    settings: {
			        centerPadding: '0',
			        slidesToShow: 1
			    }
			}
		]
	});
}

$(document).ready(function(){
	reviewsSlider('.reviews-slider');
	mainSlider('.slider');

	$('.header-burger').click(function(e) {
		e.preventDefault();
		$('.header-fixed').toggleClass('active');
		$('.main').toggleClass('hide');
		setTimeout(function() {
			$('.header-burger').toggleClass('active');
		}, 100);
	});

	if ($(window).outerWidth() > 992) {
		$('.open_search').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$('.search_form').addClass('active').find('input').focus();
			return false;
		});
	}

	if ($(window).outerWidth() <= 992) {
		$('.header-menu .catalogue a').click(function() {
			$(this).closest('ul').find('ul').slideUp();
			if ($(this).next('ul').css('display') == 'none') {
				$(this).next('ul').slideDown();
			} else {
				$(this).next('ul').slideUp();
			}
		});
	}

	$('.catalogue li').each(function() {
		if ($(this).find('ul').length) {
			$(this).addClass('parent');
		}
	});
});

jQuery(document).click(function (event) {
	if ($(window).outerWidth() > 992) {
		if ($(event.target).closest(".search_form").length) return;
		jQuery(".search_form").removeClass("active");
		$('.open_search').show();

		event.stopPropagation();
	}
});

$(window).load(function(){

});

$(window).resize(function(){

});